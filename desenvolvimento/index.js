const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const db = require("./src/configs/sequelize");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));

require("./src/user/routesuser")(app);
require("./src/posts/routesposts")(app);

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname + "/public/index.html"));
});

/*app.listen(3000, () => {
  console.log("Sever aberto");
});*/

//execute na porta principal ou 3000
app.listen(process.env.port || 3000, () => {
  console.log("Servidor Online em: http://localhost:3000/");
});