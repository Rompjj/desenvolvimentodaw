module.exports = (app) =>{
    const controller = require("./controlleruser")

    //criando new Users
    app.post('/usuarios', controller.create)

    //buscando Users
    app.get('/usuarios', controller.findAll)
}