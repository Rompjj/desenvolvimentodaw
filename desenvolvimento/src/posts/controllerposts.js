const db = require("../configs/sequelize");
const Post = require("./modelposts");
const User = require("../user/modeluser");
const { Op } = db.Sequelize;

exports.create = (req, res) => {
  Post.create(
    {
      content: req.body.content,
      userId: req.body.userId,
    },
    { include: [{ association: Post.User }] }
  )
    .then((post) => res.send(post))
    .catch((err) => console.log("Error: " + err));
};

exports.findAll = (req, res) => {
  Post.findAll({ include: User, where: {content: {[Op.like]: "%" + req.query.content + "%",},}, order: ["createdAt"] }).then((posts) =>
    res.send(posts)
  );
};

exports.update = (req, res) => {
  Post.update(
    { content: req.body.content },
    { where: { id: req.body.id } }
  ).then(res.send({ message: "post confirmado" }));
};

exports.delete = (req, res) => {
  Post.destroy({ where: { id: req.body.id } }).then((affectedRows) =>
    res.send({ message: "post confirmado", affectedRows: affectedRows })
  );
};