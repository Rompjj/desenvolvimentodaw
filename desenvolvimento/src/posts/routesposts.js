module.exports = (app) => {
    const controller = require("./controllerposts");
  
    //criando new Posts
    app.post("/post", controller.create);
  
    //buscando posts
    app.get("/post", controller.findAll);
  
    //atualiza posts
    app.put("/post", controller.update);
  
    //atualiza posts
    app.delete("/post", controller.delete);
  };