const dbconfig = require('./database.js')
const Sequelize = require('sequelize')
const sequelize = new Sequelize(dbconfig) //base que irei me conectar

//teste de coneccao
sequelize.authenticate().
    then(function(){
        console.log("Conectado com sucesso!")
}). catch(function(erro){
        console.log("Falha ao conectar!" + erro)
})

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

module.exports = db