Posts = {
  add: () => {
    let dados = {};
    dados.content = $("#content").val();
    dados.firstname = $("#firstname").val();
    dados.lastname = $("#lastname").val();

    $.ajax({
      url: "/post",
      type: "POST",
      data: dados,
      dataType: "json",
      success: Posts.template,
    });
  },

  template: (data) => {
    let comment = $("<div></div>")
      .attr("id", "comment-" + data.id)
      .attr("class", "comment")

    const content = $("<textarea></textarea>")
      .attr("class", "content")
      .html(data.content)
      .attr("disabled", true)

    const user = $("<p></p>")
      .attr("class", "user")
        if (data.user) {
          user.html(`${data.user.firstname} ${data.user.lastname}`);
        } else {
          user.html("Nome " + $("#users option:selected").text());
      }

    const date = $("<span></span>")
      .attr("class", "date")
      .html(` ${new Date(data.createdAt)
      .toLocaleDateString("pt-BR")}`)

    const btnEdit = $("<button></button>")
      .attr(
        "class",
        "btn-edit fa fa-pencil"
    );

    const btnSave = $("<button></button>")
      .attr(
        "class",
        "btn-save hidden fa fa-save"
    );

    const btnDelete = $("<button></button>")
      .attr(
        "class",
        "btn-del fa fa-trash"
    );

    $(btnEdit).on("click", (e) => Posts.textEdit(e.target));

    $(btnSave).on("click", (e) => Posts.textUpdate(e.target));

    $(btnDelete).on("click", (e) => Posts.delete(e.target));

    $(user).append(date);
    $(comment).append(content);
    $(comment).append(user);
    $(comment).append(btnEdit);
    $(comment).append(btnSave);
    $(comment).append(btnDelete);

    $("#comments").append(comment);
  },

  findAll: () => {
    console.log($("#find").val());
    $.ajax({
      url: "/post",
      type: "GET",
      data: { content: $("#find").val() },
      success: (data) => {
        $("#comments").empty();
        for (let post of data) {
          Posts.template(post);
        }
      },
      error: () => {
        console.log("Ocorreu um erro!");
      },
      dataType: "json",
    });
  },

  textEdit: (btn) => {
    const comment = $(btn).parent();
    $(comment).children("textarea").prop("disabled", false);
    $(comment).children(".btn-edit").hide();
    $(comment).children(".btn-save").show();
  },

  textUpdate: (btn) => {
    const comment = $(btn).parent();
    const id = $(comment).attr("id").replace("comment-", "");
    const content = $(comment).children("textarea").val();

    $.ajax({
      url: "/post",
      type: "PUT",
      data: {
        content: content,
        id: id,
      },
      success: (data) => {
        $(comment).children("textarea").prop("disabled", true);
        $(comment).children(".btn-edit").show();
        $(comment).children(".btn-save").hide();
      },
      error: () => {
        console.log("Ocorreu um erro!");
      },
      dataType: "json",
    });
  },

  delete: (btn) => {
    const comment = $(btn).parent();
    const id = $(comment).attr("id").replace("comment-", "");

    $.ajax({
      url: "/post",
      type: "DELETE",
      data: {
        id: id,
      },
      success: (data) => {
        $(comment).remove();
      },
      error: () => {
        console.log("Ocorreu um erro!");
      },
      dataType: "json",
    });
  },
};

User = {
  findAll: () => {
    $.ajax({
      type: "GET",
      url: "/usuarios",
      success: User.load,
      dataType: "json",
    });
  },
  load: (data) => {
    const userCombo = $("#users");
    for (user of data) {
      userCombo.append(
        $("<option></option>")
          .attr("value", user.id)
          .html(user.firstname + " " + user.lastname)
      );
    }
  },
};

$(document).ready(() => {
  Posts.findAll();
  User.findAll();
});

$("select").each(function () {
  var $this = $(this),
    numberOfOptions = $(this).children("option").length;

  $this.addClass("select-hidden");
  $this.wrap('<div class="select"></div>');
  $this.after('<div class="select-styled"></div>');

  var $styledSelect = $this.next("div.select-styled");
  $styledSelect.text($this.children("option").eq(0).text());

  var $list = $("<ul/>", {
    class: "select-options",
  }).insertAfter($styledSelect);

  for (var i = 0; i < numberOfOptions; i++) {
    $("<li/>", {
      text: $this.children("option").eq(i).text(),
      rel: $this.children("option").eq(i).val(),
    }).appendTo($list);
  }

  var $listItems = $list.children("li");

  $styledSelect.click(function (e) {
    e.stopPropagation();
    $("div.select-styled.active")
      .not(this)
      .each(function () {
        $(this).removeClass("active").next("ul.select-options").hide();
      });
    $(this).toggleClass("active").next("ul.select-options").toggle();
  });

  $listItems.click(function (e) {
    e.stopPropagation();
    $styledSelect.text($(this).text()).removeClass("active");
    $this.val($(this).attr("rel"));
    $list.hide();
  });

  $(document).click(function () {
    $styledSelect.removeClass("active");
    $list.hide();
  });
});